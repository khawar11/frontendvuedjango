describe("editcustomer", () => {
    beforeEach(() => {
        // Visit the customer list page
        cy.visit('https://vuedjango-452fc.web.app/Customer');
        cy.wait(10000);
    });
    it('edit customer data', () => {
        // cy.visit('https://vuedjango-452fc.web.app/Customer');
        cy.scrollTo('bottom');
        // cy.wait(5000);

        // Check if the customer named John is displayed
        // cy.contains('John').should('exist');
        // cy.log('Edit icon for John is clicked');

        // Click on the edit icon for the customer named John
        cy.contains('John').parent().within(() => {
            cy.get('.fa.fa-pencil.fa-lg.cursor-pointer').should('be.visible').click();
        });
        cy.log('Edit icon for John is clicked');

        // Wait for the edit modal to appear
        cy.get('#editModal').should('exist');

        // Update the company name field
        cy.get('#companyName').clear().type('New Company Name');

        // Click on the update button
        cy.get('#editModal').find('form').submit();

        // Wait for the edit modal to disappear
        cy.get('#editModal').should('not.exist');

        // Check if the updated company name is displayed
        cy.contains('New Company Name').should('exist');
    });
});
