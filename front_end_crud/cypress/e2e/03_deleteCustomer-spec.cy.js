describe('Customer List', () => {
    beforeEach(() => {
        // Visit the customer list page
        cy.visit('https://vuedjango-452fc.web.app/Customer');
        // cy.wait(5000);
    });

    it('deletes_customer_named_john', () => {
        // Check if the customer named John is displayed
        // cy.contains('John').should('exist');

        // Click on the delete icon for the customer named John
        cy.contains('John').parent().within(() => {
            cy.get('.fa.fa-trash.fa-lg.cursor-pointer').should('be.visible').click();
        });

        // Check if the deletion confirmation modal is displayed
        cy.get('#deleteModal').should('exist');

        // Check if the modal title is correct
        cy.get('#deleteModalTitle').should('contain', 'Confirm Deletion');

        // Check if the modal body text is correct
        cy.get('.modal-body').should('contain', 'Are you sure you want to delete this customer?');

        // Click on the delete button to confirm deletion
        cy.get('#deleteModal').find('form').submit();

        // Check if the deletion confirmation modal is closed
        cy.get('#deleteModal').should('not.exist');

        // Check if the customer named John is deleted from the list
        // cy.contains('John').should('not.exist');
    });
});
