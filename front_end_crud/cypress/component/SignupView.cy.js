import SignupView from '../../src/views/SignupView.vue'

describe('<SignupView />', () => {
  it('renders_with_form_fields', () => {
    // see: https://on.cypress.io/mounting-vue
    // cy.mount(SignupView)
    cy.wait(5000)
    // cy.mount('https://vuedjango-452fc.web.app/signup')
    cy.mount('http://localhost:5173/signup')
    cy.get('input[name="email"]').should('exist')
    cy.get('input[name="password"]').should('exist')
    cy.get('input[name="confirm_password"]').should('exist')
  })

  it('renders_with_submit_button', () => {
    // see: https://on.cypress.io/mounting-vue
    // cy.mount(SignupView)
    // cy.mount('https://vuedjango-452fc.web.app/signup')
    cy.mount('http://localhost:5173/signup')
    cy.get('button[type="submit"]').should('exist')
  })
  
})