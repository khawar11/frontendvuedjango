const { defineConfig } = require("cypress");

module.exports = defineConfig({
  projectId: "kcd9qz",
  component: {
    devServer: {
      framework: "vue",
      bundler: "vite",
      baseUrl: "https://vuedjango-452fc.web.app",
      defaultCommandTimeout: 10000,
    },
    
    // specPattern: "**/*.cy.js",
    specPattern: [
      "**/*.spec.cy.js",
      "e2e/01_addCustomer-spec.cy.js",
      "e2e/02_editCustomer-spec.cy.js",
      "e2e/03_deleteCustomer-spec.cy.js"
    ]
  },
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
  },
  // projectId: '35c2c2ea-7442-4de1-ba33-3dba6bc79b4f',
});
